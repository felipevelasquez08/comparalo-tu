FROM sonarsource/sonar-scanner-cli
RUN rm /opt/sonar-scanner/conf/sonar-scanner.properties
COPY sonar-project.properties /opt/sonar-scanner/conf/sonar-scanner.properties
COPY . /usr/src/
RUN chmod -R 777 test-report.xml
RUN npm run test
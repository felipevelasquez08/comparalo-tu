import {ProviderRepository} from "../repository/ProviderRepository";
import {ListUtilities} from "../../utilities/ListUtilities";
import {EmptyListException} from "../../exception/EmptyListException";
import {Provider} from "../model/entity/Provider";
import {BadIdException} from "../../exception/BadIdException";
import {StringUtilities} from "../../utilities/StringUtilities";

export class ProviderService {

    private repository: ProviderRepository

    constructor(repository: ProviderRepository) {
        this.repository = repository
    }

    async consultProviders(): Promise<Provider[]> {
        let companies = await this.repository.consultCompanies()
        if (ListUtilities.isEmpty(companies)) {
            let message: string = "List of companies is empty"
            throw new EmptyListException(message)
        }
        return companies
    }

    async consultCompanyById(providerId: string): Promise<Provider> {
        if (StringUtilities.isEmpty(providerId)) {
            let message: string = "Id is bad"
            throw new BadIdException(message)
        }
        return await this.repository.consultCompanyById(providerId)
    }

    static async newInstance(repository: ProviderRepository): Promise<ProviderService> {
        return new ProviderService(repository)
    }
}
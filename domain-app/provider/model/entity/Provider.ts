import {NumberUtilities} from "../../../utilities/NumberUtilities";
import {BadIdException} from "../../../exception/BadIdException";
import {StringUtilities} from "../../../utilities/StringUtilities";
import {EmptyValueException} from "../../../exception/EmptyValueException";

export class Provider {

    readonly id: string
    readonly name: string
    readonly icon: string

    constructor(id: string, name: string, icon: string) {
        this.id = id
        this.name = name
        this.icon = icon
        this.validate()
    }

    private validate() {
        this.validateId()
        this.validateName()
        this.validateIcon()
    }

    private validateId() {
        if (StringUtilities.isEmpty(this.id)) {
            let message = "Id is bad"
            throw new BadIdException(message)
        }
    }

    private validateName() {
        if (StringUtilities.isEmpty(this.name)) {
            const message = "Name is empty"
            throw new EmptyValueException(message)
        }
    }

    private validateIcon() {
        if (StringUtilities.isEmpty(this.icon)) {
            const message = "Icon is empty"
            throw new EmptyValueException(message)
        }
    }
}
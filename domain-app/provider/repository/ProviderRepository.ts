import {Provider} from "../model/entity/Provider";

export interface ProviderRepository {
    consultCompanies(): Promise<Provider[]>
    consultCompanyById(id: string): Promise<Provider>
}
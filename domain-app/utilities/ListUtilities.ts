export class ListUtilities {
    static isEmpty<Type>(list: Type[]): boolean {
        return list.length == 0
    }
}
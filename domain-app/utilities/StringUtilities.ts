import {NumberUtilities} from "./NumberUtilities";

export class StringUtilities {
    static isEmpty(string: string): boolean {
        return string === ""
    }

    static isEmptyOrUndefined(string: string): boolean {
        return this.isEmpty(string) || string === undefined
    }

    static isFormatPrice(string: string): boolean {
        const formatPrice: RegExp = /[^$.\d,-]*/g
        const price = string.trim().replace(formatPrice, "")
        return !this.isEmpty(price)
    }

    static toNumber(string: string): number {
        const formatPrice: RegExp = /[^\d-,]*(,\d+)?/g
        let number = string.replace(formatPrice, "")
        let isNumber = NumberUtilities.isNumber(number)
        if (isNumber) {
            return +number
        }
        return -1
    }

    static formatPriceToInt(priceString: string): number {
        let priceNumberFormat = priceString.replace(/\D+/g, '');
        return ((this.isEmpty(priceNumberFormat)) ? -1 : +priceNumberFormat)
    }

    static slugToText(slug: string): string {
        return slug.replace(/-/g, " ")
    }
}
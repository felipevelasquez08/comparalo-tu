export class NumberUtilities {

    static isNumber(number: any): boolean {
        return !isNaN(+number)
    }

    static isNegative(number: number): boolean {
        return number < 0
    }

    static isBadId(id: number): boolean {
        return this.isNegative(id)
    }
}
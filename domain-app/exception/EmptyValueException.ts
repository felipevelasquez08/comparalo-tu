import {BaseException} from "./BaseException";

export class EmptyValueException extends BaseException {
    constructor(message: string) {
        const name = "EmptyValueException"
        super(message, name);
    }
}
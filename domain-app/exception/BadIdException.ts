import {BaseException} from "./BaseException";

export class BadIdException extends BaseException {
    constructor(message: string) {
        let name = "BadIdException"
        super(message, name);
    }
}
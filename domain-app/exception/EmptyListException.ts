import {BaseException} from "./BaseException";

export class EmptyListException extends BaseException {
    constructor(message: string) {
        const name = "EmptyListException"
        super(message, name);
    }
}
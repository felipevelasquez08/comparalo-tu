import {BaseException} from "./BaseException";

export class BadFormatPriceException extends BaseException{
    constructor(message: string) {
        let name = "BadNumberFormatException"
        super(message, name);
    }
}
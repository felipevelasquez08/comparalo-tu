import {BaseException} from "./BaseException";

export class NegativeNumberException extends BaseException {
    constructor(message: string) {
        const name = "NegativeNumberException"
        super(message, name);
    }
}
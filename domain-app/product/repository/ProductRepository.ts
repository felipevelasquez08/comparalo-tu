import {Product} from "../model/valueobject/Product";

export interface ProductRepository {
    getProductsByName(productName: string): Promise<Product[]>
}
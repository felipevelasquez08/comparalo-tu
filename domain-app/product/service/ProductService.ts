import {ProductRepository} from "../repository/ProductRepository";
import {Product} from "../model/valueobject/Product";
import {StringUtilities} from "../../utilities/StringUtilities";
import {EmptyValueException} from "../../exception/EmptyValueException";

export class ProductService {

    private repository: ProductRepository

    constructor(repository: ProductRepository) {
        this.repository = repository
    }

    async getProductByName(productName: string): Promise<Product[]> {
        if (StringUtilities.isEmptyOrUndefined(productName)) {
            let message: string = "Product name is empty"
            throw new EmptyValueException(message)
        }
        return this.repository.getProductsByName(productName)
    }

    static async newInstance(repository: ProductRepository): Promise<ProductService> {
        return new ProductService(repository)
    }
}
import {StringUtilities} from "../../../utilities/StringUtilities";
import {EmptyValueException} from "../../../exception/EmptyValueException";
import {NumberUtilities} from "../../../utilities/NumberUtilities";
import {NegativeNumberException} from "../../../exception/NegativeNumberException";
import {BadFormatPriceException} from "../../../exception/BadFormatPriceException";

export class Product {

    private readonly companyName: string
    private readonly productName: string
    private readonly productImage: string
    private priceFormatNumber: number
    private readonly price: string

    constructor(companyName: string, productName: string, productImage: string, price: string) {
        this.companyName = companyName
        this.productName = productName
        this.productImage = productImage
        this.price = price
        this.priceFormatNumber = StringUtilities.formatPriceToInt(this.price)
        this.validate()
    }

    private validate() {
        this.validateCompanyName()
        this.validateProductName()
        this.validatePrice()
    }

    private validateCompanyName() {
        if (StringUtilities.isEmpty(this.companyName)) {
            const message = "Company name is empty"
            throw new EmptyValueException(message)
        }
    }

    private validateProductName() {
        if (StringUtilities.isEmpty(this.productName)) {
            const message = "Product name is empty"
            throw new EmptyValueException(message)
        }
    }

    private validatePrice() {
        this.validateEmptyPrice()
        this.validateFormatPrice()
        this.validatePriceIsNegative()
    }

    private validateEmptyPrice() {
        if (StringUtilities.isEmpty(this.price)) {
            let message = "Price is empty"
            throw new EmptyValueException(message)
        }
    }

    private validateFormatPrice() {
        let isPriceFormat = StringUtilities.isFormatPrice(this.price)
        if (!isPriceFormat) {
            const message = "Price not is number"
            throw new BadFormatPriceException(message)
        }
    }

    private validatePriceIsNegative() {
        let price = StringUtilities.toNumber(this.price)
        if (NumberUtilities.isNegative(price)) {
            const message = "Price is negative"
            throw new NegativeNumberException(message)
        }
    }

    toString() {
        const elements = [this.companyName, this.productName, this.price]
        const separator = " | "
        return elements.join(separator)
    }
}
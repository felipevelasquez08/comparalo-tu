import {initializeApp} from "firebase/app"
import {getFirestore} from "firebase/firestore";


const firebaseConfig = {
    apiKey: "AIzaSyDNp-Ni4Ek_NEbzKyRyt-3cixOzgL-QYW0",
    authDomain: "comparalo-tu.firebaseapp.com",
    projectId: "comparalo-tu",
    storageBucket: "comparalo-tu.appspot.com",
    messagingSenderId: "751891575276",
    appId: "1:751891575276:web:d89feb6073918de97bd0c9",
    measurementId: "G-246ZZDRM2G"
};

export class BaseAdapter {
    private app = initializeApp(firebaseConfig);
    protected db = getFirestore(this.app)
}
import {ProductDTO} from "../../main/common/model/dto/ProductDTO";
import {ExtractProductScraper} from "../../main/common/scraper/ExtractProductScraper";
import {DataAdapter} from "../../main/product/adapter/DataAdapter";


export class DataAdapterImpl implements DataAdapter {

    private extractProductScraper: ExtractProductScraper

    constructor(extractProductScraper: ExtractProductScraper) {
        this.extractProductScraper = extractProductScraper
    }

    async getData(productName: string): Promise<ProductDTO[]> {
        return await this.extractProductScraper.consultProductByName(productName)
    }

    static async newInstance(): Promise<DataAdapterImpl> {
        let extractProductScraper = await ExtractProductScraper.newInstance()
        return new DataAdapterImpl(extractProductScraper)
    }

}
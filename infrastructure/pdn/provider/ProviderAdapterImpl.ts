import {ProviderAdapter} from "../../main/provider/repository/ProviderAdapter";
import {ProviderDto} from "../../main/provider/repository/ProviderDto";
import {BaseAdapter} from "../common/BaseAdapter";
import {collection, CollectionReference, doc, getDoc, getDocs, query} from "firebase/firestore";

export class ProviderAdapterImpl extends BaseAdapter implements ProviderAdapter {

    private data: CollectionReference = collection(this.db, 'companies')

    async consultCompanies(): Promise<ProviderDto[]> {
        let consult = query(this.data)
        let response = await getDocs(consult)
        return response.docs.map(doc => {
            let id = doc.id
            let data = doc.data()
            return new ProviderDto(id, data.name, data.icon)
        })
    }

    async consultCompanyById(id: string): Promise<ProviderDto> {
        let consult = doc(this.data, id)
        let response = await getDoc(consult)
        if (!response.exists()) {
            throw new Error("Document not found")
        }
        let data = response.data()
        return new ProviderDto(id, data.name, data.icon)
    }

}
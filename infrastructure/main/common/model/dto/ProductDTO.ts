export class ProductDTO {

    companyName: string
    productName: string
    productImage: string
    price: string

    constructor(companyName: string, productName: string, productImage: string, price: string) {
        this.companyName = companyName
        this.productName = productName
        this.productImage = productImage
        this.price = price
    }
}
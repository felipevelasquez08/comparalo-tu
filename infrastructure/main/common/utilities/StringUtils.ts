export class StringUtils {
    static orNA(value: string): string {
        return value || "N/A"
    }

    static orEmpty(value: string): string {
        return value || ""
    }

    static orZero(value: string): string {
        return value || "0"
    }
}
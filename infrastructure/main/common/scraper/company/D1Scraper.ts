import {BaseScraper} from "./BaseScraper";
import {Page} from "puppeteer";
import {CompanyEnum} from "../model/enum/CompanyEnum";
import {ProductDTO} from "../../model/dto/ProductDTO";

export class D1Scraper extends BaseScraper {

    private readonly productElement = this.company.productElement

    async buildResponse(companyName: string): Promise<ProductDTO[]> {
        await this.waitForSelector(this.productElement.priceKey)
        await this.waitForSelector(this.productElement.imageKey)
        return this.getProducts(companyName)
    }

    static async newInstance(page: Page): Promise<D1Scraper> {
        let company = CompanyEnum.d1
        return new D1Scraper(page, company)
    }
}
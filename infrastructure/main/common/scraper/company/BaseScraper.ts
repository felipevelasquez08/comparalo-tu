import {Page} from "puppeteer";
import {CompanyEnum} from "../model/enum/CompanyEnum";
import {ProductDTO} from "../../model/dto/ProductDTO";
import {BadInitException} from "../exception/BadInitException";
import {ProductElementEnum} from "../model/enum/ProductElementEnum";
import {StringUtils} from "../../utilities/StringUtils";

export class BaseScraper {

    protected page: Page
    protected company: CompanyEnum

    constructor(page: Page, company: CompanyEnum) {
        this.page = page
        this.company = company
    }

    async consultProductByName(productName: string): Promise<ProductDTO[]> {
        let fullPath = this.getDomain().replace("{}", productName)
        await this.page.goto(fullPath)
        return this.buildResponse(this.getCompanyName())
    }

    private getDomain(): string {
        return this.company.url
    }

    private getCompanyName(): string {
        return this.company.name
    }

    async buildResponse(_companyName: string): Promise<ProductDTO[]> {
        let message = "Expected init request"
        throw new BadInitException(message)
    }

    protected async waitForSelector(selector: string) {
        await this.page.waitForSelector(selector)
    }

    protected async getProducts(companyName: string): Promise<ProductDTO[]> {
        let sectionKey = this.getProductElement().sectionKey
        let nameKey = this.getProductElement().nameKey
        let imageKey = this.getProductElement().imageKey
        let priceKey = this.getProductElement().priceKey
        return this.page.evaluate(async (companyNameParam: string, sectionKeyParam: string, nameKeyParam: string, imageKeyParam: string, priceKeyParam: string) => {
            let articles = document.querySelectorAll(sectionKeyParam)
            let products: ProductDTO[] = Array.from(articles).map(article => {
                let productNameElement = article.querySelector(nameKeyParam)
                let productName = StringUtils.orNA(productNameElement.textContent)
                let productImageElement: HTMLEmbedElement = article.querySelector(imageKeyParam)
                let productImage = StringUtils.orEmpty(productImageElement.src)
                let priceElement = article.querySelector(priceKeyParam)
                let price = StringUtils.orZero(priceElement.textContent)
                return {
                    companyName: companyNameParam,
                    productName: productName,
                    productImage: productImage,
                    price: price
                }
            })
            return products
        }, companyName, sectionKey, nameKey, imageKey, priceKey)
    }

    private getProductElement(): ProductElementEnum {
        return this.company.productElement
    }

    static async newInstance(_page: Page): Promise<BaseScraper> {
        throw new BadInitException("Bad init")
    }
}
import {BaseScraper} from "./BaseScraper";
import {Page} from "puppeteer";
import {CompanyEnum} from "../model/enum/CompanyEnum";
import {ProductDTO} from "../../model/dto/ProductDTO";

export class OlimpicaScraper extends BaseScraper {

    async buildResponse(companyName: string): Promise<ProductDTO[]> {
        await this.waitForSelector("div.vtex-product-price-1-x-sellingPrice--hasListPrice--dynamicF")
        return this.getProducts(companyName)
    }

    static async newInstance(page: Page): Promise<BaseScraper> {
        let company = CompanyEnum.olimpica
        return new OlimpicaScraper(page, company)
    }
}
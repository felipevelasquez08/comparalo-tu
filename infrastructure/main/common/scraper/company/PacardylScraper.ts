import {BaseScraper} from "./BaseScraper";
import {Page} from "puppeteer";
import {CompanyEnum} from "../model/enum/CompanyEnum";
import {ProductDTO} from "../../model/dto/ProductDTO";

export class PacardylScraper extends BaseScraper {

    async buildResponse(companyName: string): Promise<ProductDTO[]> {
        await this.waitForSelector("span.woocommerce-Price-amount")
        return this.getProducts(companyName)
    }

    static async newInstance(page: Page): Promise<BaseScraper> {
        let company = CompanyEnum.pacardyl
        return new PacardylScraper(page, company)
    }
}
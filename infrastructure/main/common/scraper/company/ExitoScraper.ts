import {BaseScraper} from "./BaseScraper";
import {Page} from "puppeteer";
import {CompanyEnum} from "../model/enum/CompanyEnum";
import {ProductDTO} from "../../model/dto/ProductDTO";

export class ExitoScraper extends BaseScraper {

    async buildResponse(companyName: string): Promise<ProductDTO[]> {
        await this.waitForSelector("span.exito-vtex-components-4-x-currencyContainer")
        return this.getProducts(companyName)
    }

    static async newInstance(page: Page): Promise<BaseScraper> {
        let company = CompanyEnum.exito
        return new ExitoScraper(page, company)
    }
}
import {BaseScraper} from "./BaseScraper";
import {CompanyEnum} from "../model/enum/CompanyEnum";
import {ProductDTO} from "../../model/dto/ProductDTO";
import {Page} from "puppeteer";

export class BoomScraper extends BaseScraper {

    async buildResponse(companyName: string): Promise<ProductDTO[]> {
        await this.waitForSelector("div.styles__ContainerTemplate-sc-1kk9itx-0")
        return this.getProducts(companyName)
    }

    static async newInstance(page: Page): Promise<BaseScraper> {
        let company = CompanyEnum.boom
        return new BoomScraper(page, company)
    }

}
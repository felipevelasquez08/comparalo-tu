export class BadCompanyNameException extends Error {
    constructor(message: string) {
        super(message);
        this.name = "BadCompanyNameException"
    }
}
export class BadUrlException extends Error {
    constructor(message: string) {
        super(message);
        this.name = "BadUrlException"
    }
}
import puppeteer, {Browser, Page} from "puppeteer";
import {ProductDTO} from "../model/dto/ProductDTO";
import {BoomScraper} from "./company/BoomScraper";
import {D1Scraper} from "./company/D1Scraper";
import {ExitoScraper} from "./company/ExitoScraper";
import {OlimpicaScraper} from "./company/OlimpicaScraper";
import {PacardylScraper} from "./company/PacardylScraper";

export class ExtractProductScraper {

    private browser: Browser
    private companyD1: D1Scraper
    private companyBoom: BoomScraper
    private companyPacardyl
    private companyExito: ExitoScraper
    private companyOlimpica

    constructor(browser: Browser) {
        this.browser = browser
    }

    async initialCompanies() {
        this.companyD1 = await D1Scraper.newInstance(await this.newPage())
        this.companyBoom = await BoomScraper.newInstance(await this.newPage())
        this.companyPacardyl = await PacardylScraper.newInstance(await this.newPage())
        this.companyExito = await ExitoScraper.newInstance(await this.newPage())
        this.companyOlimpica = await OlimpicaScraper.newInstance(await this.newPage())
    }

    async newPage(): Promise<Page> {
        return this.browser.newPage()
    }

    async consultProductByName(productName): Promise<ProductDTO[]> {
        let [productsD1
            // , productsBoom, productsPacardyl, productsExito, productsOlimpica
        ] = await Promise.all([
            this.companyD1.consultProductByName(productName),
            // this.companyBoom.consultProductByName(productName),
            // this.companyPacardyl.consultProductByName(productName),
            // this.companyExito.consultProductByName(productName),
            // this.companyOlimpica.consultProductByName(productName)
        ])
        await this.browser.close()
        return productsD1
            // .concat(productsBoom, productsPacardyl, productsExito, productsOlimpica)
    }

    static async newInstance(): Promise<ExtractProductScraper> {
        let browser = await puppeteer.launch()
        const extractProductScraper = new ExtractProductScraper(browser)
        await extractProductScraper.initialCompanies()
        return extractProductScraper
    }
}
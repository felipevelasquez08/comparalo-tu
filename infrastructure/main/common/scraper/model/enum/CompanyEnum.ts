import {ProductElementEnum} from "./ProductElementEnum";

export class CompanyEnum {
    static readonly olimpica = new CompanyEnum(1, "Olimpica", "https://www.olimpica.com/{}", ProductElementEnum.olimpica)
    static readonly pacardyl = new CompanyEnum(2, "Pacardyl", "https://pacardyl.com/?s={}&product_cat=0&post_type=product", ProductElementEnum.pacardyl)
    static readonly boom = new CompanyEnum(3, "Boom", "https://domicilios.tiendasd1.com/search?name={}", ProductElementEnum.boom)
    static readonly exito = new CompanyEnum(4, "Exito", "https://www.exito.com/huevo?_q={}&map=ft", ProductElementEnum.exito)
    static readonly d1 = new CompanyEnum(5, "D1", "https://domicilios.tiendasd1.com/search?name={}", ProductElementEnum.d1)

    private readonly _id: number
    private readonly _name: string
    private readonly _url: string
    private readonly _productElement: ProductElementEnum

    constructor(id: number, name: string, url: string, productElement: ProductElementEnum) {
        this._id = id
        this._name = name
        this._url = url
        this._productElement = productElement
    }


    get id(): number {
        return this._id;
    }

    get name(): string {
        return this._name;
    }

    get url(): string {
        return this._url;
    }

    get productElement(): ProductElementEnum {
        return this._productElement;
    }
}
export class ProductElementEnum {

    static readonly olimpica = new ProductElementEnum(
        "article.vtex-product-summary-2-x-element",
        "span.vtex-product-summary-2-x-productBrand",
        "img.vtex-product-summary-2-x-imageNormal",
        "div.vtex-product-price-1-x-sellingPrice--hasListPrice--dynamicF"
    )
    static readonly pacardyl = new ProductElementEnum(
        "div.product-card",
        "h3.woocommerce-loop-product__title",
        "a.card-img-top > img",
        "span.woocommerce-Price-amount"
    )
    static readonly boom = new ProductElementEnum(
        "div.oe_product_cart",
        "a.product_name",
        "div.oe_product_image > a > span > img",
        "span[data-oe-type='monetary']"
    )
    static readonly exito = new ProductElementEnum(
        "section.vtex-product-summary-2-x-container ",
        "span.vtex-store-components-3-x-productBrand",
        "img.vtex-product-summary-2-x-imageNormal ",
        "div.exito-vtex-components-4-x-PricePDP"
    )
    static readonly d1 = new ProductElementEnum(
        "div.card-product-vertical",
        "p.prod__name > span",
        "img.prod__figure__img",
        "p.base__price"
    )

    private readonly _sectionKey: string
    private readonly _nameKey: string
    private readonly _imageKey: string
    private readonly _priceKey: string

    constructor(sectionKey: string, nameKey: string, imageKey: string, priceKey: string) {
        this._sectionKey = sectionKey
        this._nameKey = nameKey
        this._imageKey = imageKey
        this._priceKey = priceKey
    }


    get sectionKey(): string {
        return this._sectionKey;
    }

    get nameKey(): string {
        return this._nameKey;
    }

    get imageKey(): string {
        return this._imageKey;
    }

    get priceKey(): string {
        return this._priceKey;
    }
}
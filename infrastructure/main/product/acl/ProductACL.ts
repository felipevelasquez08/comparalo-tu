import {Product} from "../../../../domain-app/product/model/valueobject/Product";
import {ProductDTO} from "../../common/model/dto/ProductDTO";

export class ProductACL {
    static fromDtoToDomain(list: ProductDTO[]): Product[] {
        return list.map(value => new Product(value.companyName, value.productName, value.productImage, value.price))
    }
}
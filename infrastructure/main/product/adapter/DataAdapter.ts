import {ProductDTO} from "../../common/model/dto/ProductDTO";

export interface DataAdapter {
    getData(productName: string): Promise<ProductDTO[]>
}
import {Product} from "../../../../domain-app/product/model/valueobject/Product";
import {ProductRepository} from "../../../../domain-app/product/repository/ProductRepository";
import {ProductACL} from "../acl/ProductACL";
import {DataAdapterImpl as DataMockAdapter} from "../../../mock/product/DataAdapterImpl";
import {DataAdapterImpl} from "../../../pdn/product/DataAdapterImpl";
import {DataAdapter} from "../adapter/DataAdapter";

export class ProductRepositoryImpl implements ProductRepository {

    private dataAdapter: DataAdapter

    constructor(dataAdapter: DataAdapter) {
        this.dataAdapter = dataAdapter
    }

    async getProductsByName(productName): Promise<Product[]> {
        let products = await this.dataAdapter.getData(productName)
        return ProductACL.fromDtoToDomain(products)
    }

    static async newInstance(): Promise<ProductRepositoryImpl> {
        const dataAdapter: DataAdapter = process.env.ENV === "MOCK"
            ? await DataMockAdapter.newInstance()
            : await DataAdapterImpl.newInstance()
        return new ProductRepositoryImpl(dataAdapter)
    }
}
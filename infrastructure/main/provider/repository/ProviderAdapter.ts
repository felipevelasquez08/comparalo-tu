import {ProviderDto} from "./ProviderDto";

export interface ProviderAdapter {
    consultCompanies(): Promise<ProviderDto[]>

    consultCompanyById(id: string): Promise<ProviderDto>
}
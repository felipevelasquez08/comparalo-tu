export class ProviderDto {
    private readonly _id: string
    private readonly _name: string
    private readonly _icon: string

    constructor(id: string, name: string, icon: string) {
        this._id = id
        this._name = name
        this._icon = icon
    }


    get id(): string {
        return this._id;
    }

    get name(): string {
        return this._name;
    }

    get icon(): string {
        return this._icon;
    }
}
import {ProviderRepository} from "../../../../domain-app/provider/repository/ProviderRepository"
import {Provider} from "../../../../domain-app/provider/model/entity/Provider"
import {ProviderAdapter} from "./ProviderAdapter";
import {ProviderDto} from "./ProviderDto";
import {ProviderMapper} from "./ProviderMapper";

export class ProviderRepositoryImpl implements ProviderRepository {

    private adapter: ProviderAdapter

    constructor(adapter: ProviderAdapter) {
        this.adapter = adapter
    }

    async consultCompanies(): Promise<Provider[]> {
        let providers: ProviderDto[] = await this.adapter.consultCompanies()
        return ProviderMapper.toDomainList(providers)
    }

    async consultCompanyById(id: string): Promise<Provider> {
        let providerDto: ProviderDto = await this.adapter.consultCompanyById(id)
        return ProviderMapper.toDomain(providerDto)
    }

    static async newInstance(adapter: ProviderAdapter): Promise<ProviderRepositoryImpl> {
        return new ProviderRepositoryImpl(adapter)
    }
}
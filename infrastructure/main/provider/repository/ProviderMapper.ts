import {ProviderDto} from "./ProviderDto";
import {Provider} from "../../../../domain-app/provider/model/entity/Provider";

export class ProviderMapper {
    static toDomain(providerDto: ProviderDto): Provider {
        return new Provider(providerDto.id, providerDto.name, providerDto.icon)
    }

    static toDomainList(providerDtos: ProviderDto[]): Provider[] {
        return providerDtos.map(providerDto => this.toDomain(providerDto))
    }
}
import {ProviderAdapter} from "../../main/provider/repository/ProviderAdapter";
import {ProviderDto} from "../../main/provider/repository/ProviderDto";
import {CompanyEnum} from "../../main/common/scraper/model/enum/CompanyEnum";

export class ProviderAdapterImpl implements ProviderAdapter {
    async consultCompanies(): Promise<ProviderDto[]> {
        return Object.values(CompanyEnum).map(value => new ProviderDto(value.id, value.name, value.name))
    }

    async consultCompanyById(id: string): Promise<ProviderDto> {
        return Object.values(CompanyEnum).map(value => new ProviderDto(value.id, value.name, value.name)).find(value => value.id === id)
    }

}
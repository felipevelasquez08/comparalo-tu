import {StringUtilities} from "../../../domain-app/utilities/StringUtilities";
import {ProductDTO} from "../../main/common/model/dto/ProductDTO";
import {DataAdapter} from "../../main/product/adapter/DataAdapter";

export class DataAdapterImpl implements DataAdapter {

    private products: ProductDTO[] = [
        new ProductDTO("ARA", "Manzanas", "manzanas.jpg", "2.99"),
        new ProductDTO("D1", "Pechugas de pollo", "pechugas.jpg", "6.49"),
        new ProductDTO("EURO", "Leche", "leche.jpg", "3.29"),
        new ProductDTO("Olimpica", "Huevos", "huevos.jpg", "1.99"),
        new ProductDTO("Exito", "Pan integral", "pan_integral.jpg", "2.49"),
        new ProductDTO("ARA", "Yogurt", "yogurt.jpg", "1.79"),
        new ProductDTO("D1", "Atún enlatado", "atun.jpg", "1.99"),
        new ProductDTO("EURO", "Lechuga", "lechuga.jpg", "0.99"),
        new ProductDTO("Olimpica", "Papas", "papas.jpg", "2.99"),
        new ProductDTO("Exito", "Tomates", "tomates.jpg", "2.49"),
        new ProductDTO("ARA", "Carne molida", "carne_molida.jpg", "4.99"),
        new ProductDTO("D1", "Arroz", "arroz.jpg", "3.49"),
        new ProductDTO("EURO", "Frijoles", "frijoles.jpg", "1.99"),
        new ProductDTO("Olimpica", "Aceite de oliva", "aceite_oliva.jpg", "6.99"),
        new ProductDTO("Exito", "Queso cheddar", "queso_cheddar.jpg", "4.99"),
        new ProductDTO("ARA", "Mantequilla de maní", "manequeria_mani.jpg", "3.99"),
        new ProductDTO("D1", "Té verde", "te_verde.jpg", "2.99"),
        new ProductDTO("EURO", "Avena", "avena.jpg", "2.49"),
        new ProductDTO("Olimpica", "Sandía", "sandia.jpg", "4.99"),
        new ProductDTO("Exito", "Chocolate negro", "chocolate_negro.jpg", "3.49"),
    ];

    async getData(productName: string): Promise<ProductDTO[]> {
        if (StringUtilities.isEmpty(productName)) {
            return this.products
        }
        return this.products.filter(product => product.productName.toLowerCase().includes(productName.toLowerCase()));
    }

    static async newInstance(): Promise<DataAdapterImpl> {
        return new DataAdapterImpl()
    }

}
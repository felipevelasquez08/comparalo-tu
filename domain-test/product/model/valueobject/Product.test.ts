import {expect} from "chai";
import {BadFormatPriceException} from "../../../../domain-app/exception/BadFormatPriceException";
import {EmptyValueException} from "../../../../domain-app/exception/EmptyValueException";
import {NegativeNumberException} from "../../../../domain-app/exception/NegativeNumberException";
import {Product} from "../../../../domain-app/product/model/valueobject/Product";
import {ProductBuilder} from "../../../databuilder/product/model/valueobject/ProductBuilder";

describe(`Given the creation a Product`, () => {

    it(`When CompanyName is empty Then return EmptyValueException`, () => {
        let emptyCompanyName = ""
        let comparisonDetailBuilder = new ProductBuilder()
        comparisonDetailBuilder.withCompanyName(emptyCompanyName)
        try {
            comparisonDetailBuilder.build()
            expect.fail("Expected exception empty name.")
        } catch (error) {
            expect(error).to.be.instanceof(EmptyValueException)
        }
    });

    it(`When ProductName is empty Then return EmptyValueException`, () => {
        let emptyProductName = ""
        let comparisonDetailBuilder = new ProductBuilder()
        comparisonDetailBuilder.withProductName(emptyProductName)
        try {
            comparisonDetailBuilder.build()
            expect.fail("Expected exception empty name.")
        } catch (error) {
            expect(error).to.be.instanceof(EmptyValueException)
        }
    });

    it(`When Price is bad format Then return BadNumberFormatException`, () => {
        let badPriceFormat = ""
        let comparisonDetailBuilder = new ProductBuilder()
        comparisonDetailBuilder.withPrice(badPriceFormat)
        try {
            comparisonDetailBuilder.build()
            expect.fail("Expected exception empty value")
        } catch (error) {
            expect(error).to.be.instanceof(EmptyValueException)
        }
    })

    it(`When Price is letter Then return BadNumberFormatException`, () => {
        let badPriceFormat = "aa"
        let comparisonDetailBuilder = new ProductBuilder()
        comparisonDetailBuilder.withPrice(badPriceFormat)
        try {
            comparisonDetailBuilder.build()
            expect.fail("Expected exception bad number")
        } catch (error) {
            expect(error).to.be.instanceof(BadFormatPriceException)
        }
    })

    it(`When Price is negative Then return NegativeNumberException`, () => {
        let negativePrice = "-1"
        let comparisonDetailBuilder = new ProductBuilder()
        comparisonDetailBuilder.withPrice(negativePrice)
        try {
            comparisonDetailBuilder.build()
            expect.fail("Expected exception empty name.")
        } catch (error) {
            expect(error).to.be.instanceof(NegativeNumberException)
        }
    });

    it(`When correct fields Then return Object`, () => {
        let comparisonDetailBuilder = new ProductBuilder()
        let comparisonDetail = comparisonDetailBuilder.build()
        expect(comparisonDetail).to.be.instanceof(Product)
    });
});
import {expect} from "chai";
import {EmptyValueException} from "../../../domain-app/exception/EmptyValueException";
import {ProductService} from "../../../domain-app/product/service/ProductService";
import {ProductMockRepository} from "./ProductMockRepository";

let productService: ProductService

beforeEach(async () => {
    let ProductRepository = await ProductMockRepository.newInstance()
    productService = await ProductService.newInstance(ProductRepository)
})

describe(`Given the consultation of all comparison detail by ProductName`, () => {

    it(`When the ProductName is empty Then return EmptyValueException`, async () => {
        let productName = ""
        try {
            await productService.getProductByName(productName)
            expect.fail("Expected exception empty name")
        } catch (error) {
            expect(error).to.be.instanceof(EmptyValueException)
        }
    });

    it(`When the ProductName is undefined Then return EmptyValueException`, async () => {
        let productName = undefined
        try {
            await productService.getProductByName(productName)
            expect.fail("Expected exception empty name")
        } catch (error) {
            expect(error).to.be.instanceof(EmptyValueException)
        }
    });

    it('When the Consult not failed Then return products', async () => {
        let productName = "huevo"
        let comparisonList = await productService.getProductByName(productName)
        expect(comparisonList).to.be.instanceof(Array)
    });

})
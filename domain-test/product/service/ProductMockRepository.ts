import {Product} from "../../../domain-app/product/model/valueobject/Product";
import {ProductRepository} from "../../../domain-app/product/repository/ProductRepository";

export class ProductMockRepository implements ProductRepository {
    async selectAllComparisonByProductName(_productName: string): Promise<Product[]> {
        return Promise.resolve([]);
    }

    static async newInstance(): Promise<ProductMockRepository> {
        return new ProductMockRepository()
    }

}
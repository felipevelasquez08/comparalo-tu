import {Provider} from "../../../../../domain-app/provider/model/entity/Provider"

export class ProviderBuilder {
    private id = 1
    private name = "D1"
    private icon = "https://ecommerce-image-catalog.s3.amazonaws.com/D1/D1logo.png"

    withId(id: number): ProviderBuilder {
        this.id = id
        return this
    }

    withName(name: string): ProviderBuilder {
        this.name = name
        return this
    }

    withIcon(icon: string): ProviderBuilder {
        this.icon = icon
        return this
    }

    build(): Provider {
        return new Provider(this.id, this.name, this.icon)
    }

}
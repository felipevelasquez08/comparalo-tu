import {Product} from "../../../../../domain-app/product/model/valueobject/Product"

export class ProductBuilder {

    private companyName = "D1"
    private productName = "Arroz"
    private productImage = "https://exitocol.vtexassets.com/arquivos/ids/4690157-500-auto?v=637390211791630000&width=500&height=auto&aspect=true"
    private price = "8000"

    withCompanyName(companyName: string): ProductBuilder {
        this.companyName = companyName
        return this
    }

    withProductName(productName: string): ProductBuilder {
        this.productName = productName
        return this
    }

    withPrice(price: string): ProductBuilder {
        this.price = price
        return this
    }

    build(): Product {
        return new Product(this.companyName, this.productName, this.productImage, this.price)
    }
}
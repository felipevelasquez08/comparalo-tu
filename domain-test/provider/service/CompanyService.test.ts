import {expect} from "chai";
import {EmptyListException} from "../../../domain-app/exception/EmptyListException";
import {CompanyRepository} from "../../../domain-app/provider/repository/ProviderRepository";
import {CompanyService} from "../../../domain-app/provider/service/ProviderService";
import {ProviderMockRepository} from "./ProviderMockRepository";

let companyRepositoryImpl: CompanyRepository

beforeEach(async () => {
    companyRepositoryImpl = await ProviderMockRepository.newInstance()
})

describe("Company Service Test", () => {

    it('givenTheConsultCompanies_whenAllIsCorrect_thenReturnCompanies', async () => {
        let companyService = new CompanyService(companyRepositoryImpl)
        try {
            await companyService.consultCompanies()
            expect.fail("Expected exception empty list")
        } catch (error) {
            expect(error).to.be.instanceof(EmptyListException)
        }
    });
})
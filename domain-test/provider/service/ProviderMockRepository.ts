import {Provider} from "../../../domain-app/provider/model/entity/Provider";
import {ProviderRepository} from "../../../domain-app/provider/repository/ProviderRepository";

export class ProviderMockRepository implements ProviderRepository {
    async consultCompanies(): Promise<Provider[]> {
        return Promise.resolve([]);
    }

    consultCompanyById(id: number): Promise<Provider> {
        return Promise.resolve(undefined);
    }

    static async newInstance(): Promise<ProviderMockRepository> {
        return new ProviderMockRepository()
    }
}
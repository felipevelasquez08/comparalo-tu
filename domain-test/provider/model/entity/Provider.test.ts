import {BadIdException} from "../../../../domain-app/exception/BadIdException";
import {EmptyValueException} from "../../../../domain-app/exception/EmptyValueException";
import {ProviderBuilder} from "../../../databuilder/provider/model/entity/ProviderBuilder";

import {expect} from 'chai';
import 'mocha';
import assert from "assert";
import {Provider} from "../../../../domain-app/provider/model/entity/Provider";

describe("Company test", () => {

    it("givenTheCreationCompany_whenIdIsBad_thenReturnIsNotNumberException", () => {
        let badId = -1
        let companyBuilder = new ProviderBuilder()
        companyBuilder.withId(badId)
        try {
            companyBuilder.build()
            expect.fail("Expected exception bad id.")
        } catch (error) {
            expect(error).to.be.an.instanceof(BadIdException)
        }
    });

    it("givenTheCreationCompany_whenNameIsEmpty_thenReturnEmptyValueException", () => {
        let emptyName = ""
        let companyBuilder = new ProviderBuilder()
        companyBuilder.withName(emptyName)
        try {
            companyBuilder.build()
            assert.fail("Expected exception empty name.")
        } catch (error) {
            assert(error instanceof EmptyValueException)
        }
    });

    it("givenTheCreationCompany_whenIconIsEmpty_thenReturnEmptyValueException", function () {
        let emptyIcon = ""
        let companyBuilder = new ProviderBuilder()
        companyBuilder.withIcon(emptyIcon)
        try {
            companyBuilder.build()
            assert.fail("Expected exception empty icon.")
        } catch (error) {
            assert(error instanceof EmptyValueException)
        }
    });

    it("givenTheCreationCompany_whenCorrectFields_thenReturnObject", function () {
        let companyBuilder = new ProviderBuilder()
        let company = companyBuilder.build()
        assert(company instanceof Provider)
    });

});
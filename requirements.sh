rm -rf node_modules
rm -f package-lock.json

npm cache clean --force

npm install --save typescript
npm install ts-node --save-dev
npm install --save @types/node

# Body request and response
npm install --save express
npm install --save @types/express

# Scraper
npm install --save puppeteer

# Test
npm install --save-dev chai
npm install --save-dev mocha
npm install --save-dev @types/chai
npm install --save-dev @types/mocha
npm install --save-dev jest-environment-jsdom
npm install --save-dev jest-sonar-reporter

# HTTP Status Codes
npm install http-status-codes --save

# Firebase
npm install firebase
npm i @firebase/firestore
npm install firebase-admin --save
import {ResponseCT} from "../../common/model/ResponseCT";
import {ProductService} from "../../../domain-app/product/service/ProductService";
import {ProductRepositoryImpl} from "../../../infrastructure/main/product/repository/ProductRepositoryImpl";
import {BaseException} from "../../../domain-app/exception/BaseException";

export class ProductPresenter {

    private service: ProductService

    async consultProducts(productName: string) {
        // try {
            const code = 200
            let products = await this.service.getProductByName(productName)
            return new ResponseCT(code, "OK", products)
        // } catch (error) {
        //     if (error.constructor === BaseException) {
        //         const code: number = 404
        //         return new ResponseCT(code, error.message)
        //     } else {
        //         const code: number = 400
        //         return new ResponseCT(code, error.message)
        //     }
        // }
    }

    static async newInstance(): Promise<ProductPresenter> {
        let presenter: ProductPresenter = new ProductPresenter()
        let repository: ProductRepositoryImpl = await ProductRepositoryImpl.newInstance()
        presenter.service = await ProductService.newInstance(repository)
        return presenter
    }
}
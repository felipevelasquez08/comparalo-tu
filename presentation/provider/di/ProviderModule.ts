import {ProviderService} from "../../../domain-app/provider/service/ProviderService";
import {ProviderRepository} from "../../../domain-app/provider/repository/ProviderRepository";
import {ProviderRepositoryImpl} from "../../../infrastructure/main/provider/repository/ProviderRepositoryImpl";
import {ProviderAdapter} from "../../../infrastructure/main/provider/repository/ProviderAdapter";
import {
    ProviderAdapterImpl as ProviderMockAdapterImpl
} from "../../../infrastructure/mock/provider/ProviderAdapterImpl";
import {
    ProviderAdapterImpl as ProviderPdnAdapterImpl
} from "../../../infrastructure/pdn/provider/ProviderAdapterImpl";

export class ProviderModule {
    static async buildService(): Promise<ProviderService> {
        let providerAdapter: ProviderAdapter = new ProviderMockAdapterImpl()
        if (process.env.ENV === "PDN") {
            providerAdapter = new ProviderPdnAdapterImpl()
        }
        let providerRepository: ProviderRepository = await ProviderRepositoryImpl.newInstance(providerAdapter)
        return await ProviderService.newInstance(providerRepository)
    }
}
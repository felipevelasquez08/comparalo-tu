import {BasePresenter} from "../common/BasePresenter";
import {ProviderModule} from "./di/ProviderModule";
import {ProviderService} from "../../domain-app/provider/service/ProviderService";
import {ResponseCT} from "../common/model/ResponseCT";
import {Provider} from "../../domain-app/provider/model/entity/Provider";
import {BaseException} from "../../domain-app/exception/BaseException";

export class ProviderPresenter extends BasePresenter {

    private service: ProviderService

    constructor(service: ProviderService) {
        super();
        this.service = service
    }

    async consultProviders(): Promise<ResponseCT<Provider[], BaseException>> {
        return await this.launch(async (): Promise<Provider[]> =>
            await this.service.consultProviders()
        )
    }

    async consultProviderByProviderId(providerId: string): Promise<ResponseCT<Provider, BaseException>> {
        return await this.launch(async (): Promise<Provider> =>
            await this.service.consultCompanyById(providerId)
        )
    }

    static async newInstance(): Promise<ProviderPresenter> {
        return new ProviderPresenter(await ProviderModule.buildService())
    }
}
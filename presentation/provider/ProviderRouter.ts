import express, {Request, Response} from "express";
import {ProviderPresenter} from "./ProviderPresenter";
import {ResponseCT} from "../common/model/ResponseCT";
import {Provider} from "../../domain-app/provider/model/entity/Provider";
import {BaseException} from "../../domain-app/exception/BaseException";

const providerRouter = express.Router();

providerRouter.get("/", async (_req: Request, res: Response) => {
    const providerPresenter: ProviderPresenter = await ProviderPresenter.newInstance();
    const response: ResponseCT<Provider[], BaseException> = await providerPresenter.consultProviders();
    res.status(response.code)
    res.json(response);
});

providerRouter.get("/:id", async (req: Request, res: Response) => {
    const id: string = req.params.id;
    const providerPresenter: ProviderPresenter = await ProviderPresenter.newInstance();
    const response: ResponseCT<Provider, BaseException> = await providerPresenter.consultProviderByProviderId(id);
    res.status(response.code)
    res.json(response);
})

export default providerRouter;
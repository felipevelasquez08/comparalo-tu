import {BaseException} from "../../../domain-app/exception/BaseException";

export class ResponseCT<T, Error> {
     readonly code: number;
     readonly message: string;
     readonly result: T;
     readonly error: Error;

    constructor(code: number, message: string, result: T = null, error: Error = null) {
        this.code = code;
        this.message = message;
        this.result = result;
        this.error = error;
    }
}
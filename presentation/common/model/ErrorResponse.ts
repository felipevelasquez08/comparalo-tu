export class ErrorResponse<T> extends Error {
    private readonly _code: number;
    private readonly _message: string;
    private readonly _value: T;

    constructor(code: number, message: string, value: T) {
        super(message);
        this._code = code;
        this._message = message;
        this._value = value;
    }

    get code(): number {
        return this._code;
    }

    get message(): string {
        return this._message;
    }

    get value(): T {
        return this._value;
    }
}
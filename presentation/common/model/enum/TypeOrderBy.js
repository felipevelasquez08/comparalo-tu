module.exports = class TypeOrderBy {
    static orderByPrice = new TypeOrderBy("price")

    constructor(name) {
        this.name = name
    }
}
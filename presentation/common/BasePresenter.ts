import {ResponseCT} from "./model/ResponseCT";
import {ReasonPhrases, StatusCodes} from "http-status-codes";
import {BaseException} from "../../domain-app/exception/BaseException";

export class BasePresenter {
    async launch<T>(execute: () => Promise<T>): Promise<ResponseCT<T, BaseException>> {
        try {
            return new ResponseCT(StatusCodes.OK, ReasonPhrases.OK, await execute())
        } catch (error) {
            return new ResponseCT(StatusCodes.BAD_REQUEST, ReasonPhrases.BAD_REQUEST, null, error.message)
        }
    }
}
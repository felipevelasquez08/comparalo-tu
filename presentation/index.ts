import express, {Application, Request, Response} from "express"
import {ManageURL} from "./ManageURL"
import {ProductPresenter} from "./presenter/product/ProductPresenter";
import ProviderRouter from "./provider/ProviderRouter";


const app: Application = express()
const PORT = process.env.PORT || 8000

app.use('/providers', ProviderRouter)

app.get(ManageURL.Product.products, async (req: Request, res: Response) => {
    let productName: string = req.query.name as string
    let productPresenter: ProductPresenter = await ProductPresenter.newInstance()
    const response = await productPresenter.consultProducts(productName)
    res.status(response.code).send(response)
})

app.listen(PORT)